import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate, CanActivateChild } from '@angular/router';

// Components
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './service/auth-guard/auth-guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },

  /*{ path: '', 
  component: HomeComponent, 
  children: [
    {':id', component: ItemComponent}
  ]}, */

  /* { path: 'profile/:id' , canActivate: [AuthGuard], component: ProfileComponent}, */

  /* { path: 'services',
       canActivateChild: [AuthGuard],
       component: ServicesComponent 
       children: [
        {':id', component: ServiceComponent}
       ]
     }, */

  { path: '**', redirectTo: '' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
