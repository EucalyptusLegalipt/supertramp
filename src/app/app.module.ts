import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// StoreModule
import { StoreModule } from '@ngrx/store';

// StoreRouterConnectingModule
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';

// StoreRouterConnectingModule
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { appReducers } from './store/reducers/app.reducers';
import { HomeComponent } from './components/home/home.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { SideNavigationComponent } from './components/side-navigation/side-navigation.component';
import { AuthGuard } from './service/auth-guard/auth-guard.service';
import { SerializerService } from './service/serializer/serializer.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    SideNavigationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(appReducers),
    StoreDevtoolsModule.instrument({
      maxAge: 5
    }),
    StoreRouterConnectingModule.forRoot(),
    HttpClientModule
  ],
  providers: [
    AuthGuard,
    {
      provide: RouterStateSerializer,
      useClass: SerializerService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }