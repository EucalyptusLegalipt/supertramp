import { RouterStateSnapshot, Params } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';

/* Type of Serializer */
 interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
};

export class SerializerService implements RouterStateSerializer<RouterStateUrl> {

  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    console.log(routerState)
    const {
      url,
      root: {
        queryParams,
        params 
      }
    } = routerState;
    return { url, queryParams, params };
  }

}