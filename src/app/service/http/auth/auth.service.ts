import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError} from 'rxjs/operators';
import { host } from '../../../shared/config';
import { CHelpers } from '../../../shared/helpers/helpers';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  isAuthenticated(): Observable<any> {
  const headers = CHelpers.getHeader();
   return this.http.get(host.url + '/auth/authorization', { headers }).pipe(map((data: any) => {
      // Change data if it needed. 
      return data;
    }),
    // Error handling
    catchError(CHelpers.handleError) );

  }

  authorization(email, password) {
    const headers = CHelpers.getHeader();
    return this.http.post(host.url + '/auth/authorization', { email: email, password: password }, { headers }).pipe(
      map(data => {
        // Do somthing with data 
      return data;
    }), 
    // Error handling
    catchError(CHelpers.handleError) 
    );
  }

  logOut() {

  }

}
