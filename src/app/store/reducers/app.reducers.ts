import { ActionReducerMap } from '@ngrx/store';
import { IAppState } from '../state/app.state';
import { routerReducer } from '@ngrx/router-store';

export const appReducers: ActionReducerMap<IAppState> = {
 // some: someReducers,
 router: routerReducer
};

