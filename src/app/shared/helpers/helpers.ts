import { HttpErrorResponse } from '@angular/common/http';
import { throwError} from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

export class CHelpers {

  constructor() { }

  public static getHeader(): HttpHeaders {
    /*
    let token = localStorage.getItem('token');
    if (!token) {
      token = "";
    }
    */

    return new HttpHeaders()
      .set("Seat-Excess-Header", /*token*/ "token");
  }

  public static handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,

      if ([401, 403, 500].indexOf(error.status) !== -1) {
        console.error( error.error.message);
        localStorage.setItem('token', '');
        localStorage.setItem('auth', 'false');
        console.log([401, 403, 500].indexOf(error.status)) 
      }

      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };

}